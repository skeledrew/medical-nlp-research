#! /usr/bin/env python3

import sys

import rpyc


class SingletonServer(rpyc.server.Server):
    '''
    A server that does everything in a single thread.
    '''

    def _accept_method(self, sock):
        try:
            self._authenticate_and_serve_client(sock)

        except Exception as e:
            print('Something broke: ' + repr(e))

class MemoryService(rpyc.Service):

    def on_connect(self):
        pass
        if not '_blob' in globals(): self._blob = {}

    def on_disconnect(self):
        pass

    def exposed_blob(self, caller_id, key, data=None):
        global blob
        if not caller_id or (not caller_id in blob and not data): return ValueError('Invalid caller_id')
        if not data: return blob[caller_id].get(key, ValueError('Specified key does not exist'))
        if not caller_id in blob: blob[caller_id] = {}
        blob[caller_id][key] = data
        return True

blob = {}


if __name__ == '__main__':
    port = 9999 if len(sys.argv) < 2 else int(sys.argv[1])
    from rpyc.utils.server import ThreadedServer

    t = SingletonServer(MemoryService, 'localhost', port=port)
    print('Running MemoryService on port %s. Use Ctrl+C to quit.' % (port))
    t.start()
    print('Killing MemoryService.')
