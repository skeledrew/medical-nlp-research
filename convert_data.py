#! /usr/bin/env python3


import re

from common import *


def fix_mrns(src_path, dst_path, col, sep=','):
    """Fix truncated MRNs in SV files."""
    sv_iter = get_separated_values_file_iterator(src_path, sep)

    for row in sv_iter:
        mrn = row[col]
        if mrn.isdigit() and len(mrn) < 7:
            mrn = ('0' * (7 - len(mrn))) + mrn
            row[col] = mrn
        open(dst_path, 'a').write(f'{sep}'.join(row) + '\n')
    return

def split_svf_by_col(src_path, dst_path, sep='|', ref_col=0, ext='txt'):
    """Take a SV file and split into multiple files by reference column."""
    sv_iter = get_separated_values_file_iterator(src_path, sep)
    ensure_dirs(dst_path)
    # TODO: ensure empty destination
    tot_entries = 0

    for row in sv_iter:
        open(f'{dst_path}/{row[ref_col]}.{ext}', 'a').write(f'{sep}'.join(row) + '\n')
        tot_entries += 1
    print(f'Found {tot_entries} entries.')
    return tot_entries

def svfs_to_json(src_path, dst_path, sep='|', ad_path='', ad_sep=',', ad_key=None, ad_targ=None, ad_targ_map={}, overwrite=False):
    """Take a series of SV files and convert to JSON."""
    f_paths = get_file_list(src_path, True)
    ensure_dirs(dst_path)
    assoc_data = None
    tot_entries = 0
    tot_files = 0

    if ad_path:
        ad_iter = get_separated_values_file_iterator(ad_path, ad_sep)
        assoc_data = {row[ad_key]: row for row in ad_iter} if isinstance(ad_key, int) else [row for row in ad_iter]
    ext_rx = re.compile(r'\.\w+$')
    if isinstance(ad_targ_map, str):
        ad_targ_map = str_to_dict(ad_targ_map)

    for fp in f_paths:
        f_name = fp.split('/')[-1]
        content_key = ext_rx.sub('', f_name)
        f_name = ext_rx.sub('.json', f_name)
        if not overwrite and os.path.exists(f'{dst_path}/{f_name}'): continue
        svf_iter = get_separated_values_file_iterator(fp, sep)
        sv_content = [row for row in svf_iter]
        tot_entries += len(sv_content)

        if assoc_data and content_key in assoc_data:
            ad = assoc_data.get(content_key)
            if ad_targ and ad_targ_map:
                ad[ad_targ] = ad_targ_map[ad[ad_targ]]
            sv_content.append(ad)
        save_json(sv_content, f'{dst_path}/{f_name}')
        tot_files += 1
    print(f'Processed {tot_entries} entries in {tot_files} files.')
    return

def json_to_skldir(src_path, dst_path, content_col, class_col, filter_list=None, filter_col=None, ext='txt', blacklist=False):
    j_files = get_file_list(src_path, True)
    ext_rx = re.compile(r'\.\w+')
    tot_entries = 0
    written_entries = 0
    blacklisted_entries = 0
    classes_seen = []
    filters_found = {}

    for jfp in j_files:
        j_data = load_json(jfp)
        class_ = j_data[-1][class_col]
        f_name = ext_rx.sub(f'.{ext}', jfp.split('/')[-1])
        class_path = f'{dst_path}/{class_}'

        if not class_ in classes_seen:
            ensure_dirs(class_path)
            classes_seen.append(class_)

        for entry in j_data[:-1]:
            tot_entries += 1
            filter_text = entry[filter_col].strip().lower()

            if filter_list and isinstance(filter_col, int) and filter_text in filter_list:
                if not filter_text in filters_found:
                    filters_found[filter_text] = 0
                filters_found[filter_text] += 1

                if blacklist:
                    blacklisted_entries += 1
                    continue

                else:
                    written_entries += 1
                    open(f'{class_path}/{f_name}', 'a').write(entry[content_col] + '\n')
    print('\n'.join([f'{f}: {filters_found.get(f)}' for f in filters_found]))
    write_log(f'Processed {tot_entries} and wrote {written_entries} to dataset, ignoring {blacklisted_entries} due to blacklist.')
    return filters_found

def count_filter_freqs(src_path, filter_list, filter_col, year_col=None, is_valid=None, printerval=1000):
    j_files = get_file_list(src_path, True)
    filters_found = {}
    tot_entries = 0
    tot_files = len(j_files)
    proc_files = 0
    year_rx = re.compile(r'(?P<year>20\d\d)')
    bad_files = []

    for jfp in j_files:
        j_data = None

        try:
            j_data = load_json(jfp)

        except Exception as e:
            bad_files.append([jfp, e])
            continue
        proc_files += 1

        for entry in j_data:
            tot_entries += 1
            if callable(is_valid) and not is_valid(entry): continue
            filter_text = entry[filter_col].strip().lower()

            if isinstance(filter_list, list) and filter_text in filter_list or filter_list == 'all':
                if isinstance(year_col, int):
                    year = year_rx.match(entry[year_col])
                    year = year.groupdict().get('year') if year else entry[year_col]
                    filter_text += ' :: ' + year
                if not filter_text in filters_found:
                    filters_found[filter_text] = 0
                filters_found[filter_text] += 1
            if printerval and tot_entries % printerval == 0:
                print(f'Already processed {tot_entries} entries, {proc_files} of {tot_files} files.')
                #print('\n'.join([f'{f}: {filters_found.get(f)}' for f in filters_found]))
    print('\n'.join([f'{f}: {filters_found.get(f)}' for f in filters_found]))
    print(f'Total of {tot_entries} entries in {tot_files} processed.')
    return filters_found, bad_files
