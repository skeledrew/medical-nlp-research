#! /usr/bin/env python3

"""
Get various statistics...
"""


import pdb

from os.path import exists

import click

from common import *


def _read_notes(note_types='', cls_diags=''):
    anwd = load_json(allNotesWithDiags)
    ntype = load_text(note_types).lower() if note_types else None

    for mrn in anwd:
        data = anwd[mrn]
        if not len(data[-1]) == 15: data.append([None] * 15)  # missing audit hack
        audit = data.pop()
        cls_diag = (audit[-2] or '').lower()
        if cls_diags and not cls_diag in cls_diags: continue

        for note in data:
            # yield each patient encounter

            try:
                if not (ntype and note[-2].lower() in ntype): continue
                yield mrn, cls_diag, note

            except:
                pass


@click.group()
def cli():
    """Cli to Python-based operations."""
    return

@cli.command()
def test():
    """Ensure click is actually called."""
    click.echo('Runs fine!')
    return

@cli.group()
def count():
    """Count things."""
    return

@count.command('records')
@click.argument('note_types', default='')
@click.argument('diags')
@click.argument('filter_dir', default='') #, help='Directory to use notes from as MRN filter')
def count_records(note_types, diags, filter_dir):
    """Count records of given note type and diagnosis."""
    click.echo(f'Counting {note_types} records in `all_notes_with_diags` with filter {filter_dir}')
    diags = diags.split('+')
    notes = _read_notes(note_types, diags)
    if filter_dir == '-': filter_dir = ''
    tot_recs = {}

    for mrn, diag, _ in notes:
        if filter_dir and not exists(f'{filter_dir}/{diag}/{mrn}.txt'): continue
        if not diag in tot_recs: tot_recs[diag] = 0
        tot_recs[diag] += 1
    print(f'Record distribution: {tot_recs}')


@cli.group()
def extract():
    """Extract things."""
    return

@extract.command('notes')
@click.argument('note_types')
@click.argument('diags')
@click.argument('dest_dir')
def extract_notes(note_types, diags, dest_dir):
    """Extract patient notes from JSON to specified folder."""
    diags = diags.split('+')
    notes = _read_notes(note_types, diags)

    for mrn, diag, note in notes:
        if not diag in diags: continue
        ensure_dirs(f'{dest_dir}/{diag}')

        with open(f'{dest_dir}/{diag}/{mrn}.txt', 'a') as pf:
            pf.write(f'{note[-1]}\n')
    return

@cli.group()
def retrain():
    """Self-training."""
    return

@retrain.command('reclass')
def reclassify():
    """TODO: Move misclassifications over a given threshold to create a new dataset."""
    return

@cli.group()
def filter():
    """Filter files for patterns, etc."""
    return

@filter.command('csv')
@click.argument('filter')
@click.argument('src')
@click.argument('dest')
def filter_csv(filt, src, dest):
    """Filter CSV files."""
    filts = str_to_dict(filt, main_sep='%', map_sep='~')
    op = filt.get('op')
    rx = filt.get('rx')
    ensure_dirs('/'.join(dest.split('/')[:-2]))

    with open(src) as cin, open(dest, 'w', newline='') as cout:
        csv_reader = csv.reader(cin)
        csv_writer = csv.writer(cout)

        for row in csv_reader:
            if rx and not re.match(rx, ','.join(row)): continue

            if op:
                #fields = ?
                if not eval(f'{row[3]}{op}'): continue
            csv_writer.writerow(row)
    return


@cli.group()
def convert():
    """Convert data among formats."""
    return

@convert.command('skldir2csv')
@click.argument('src_dir')
@click.argument('save_file')
@click.argument('header')
@click.option('--sep', default=',')
def skldir_to_csv(src_dir, save_file, header, sep):
    """Convert a sklearn dir to a CSV."""
    success = False
    opts = {'header': header, 'sep': sep}

    try:
        success = save_skldir(load_skldir(src_dir), save_file, opts=opts)

    except Exception as e:
        print(f'Something broke during conversion: {repr(e)}')
        pdb.post_mortem()
    return


if __name__ == '__main__':

    try:
        cli()

    except Exception as e:
        print(f'Something broke in click: {repr(e)}')
        pdb.post_mortem()
